import os
import random

import pygame
from pygame.locals import *

pygame.init()
screen = pygame.display.set_mode((600,600))

#Titulo do Game
pygame.display.set_caption('Neural Network - ADS05 - THONE CARDOSO DE ARAUJO')


#Carregar componentes e iniciar variáveis para mostrar o placar do jogo
pygame.font.init()
score_value = 0
total = 0
font_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "fonts", "Evogria.otf")
font = pygame.font.Font(font_path, 24)

#Posição onde o placar estará posicionado
textX = 10
textY = 10

#Função que renderiza a Pontuação na tela do jogo
def show_score(x,y):
    score = font.render("Score: " + str(score_value) + " de " + str(total), True, (255,255,255))
    screen.blit(score, (x, y))

#Posição aleatória onde começa a cair as maçãs
def on_grid_random():
    x = random.randint(0,590)
    return x//10 * 10

#verifica se a cesta esta posicionada para aparar a maçã
def collision(c1, c2):
    for c in c1:
        if((c[0]==c2[0])and (c[1] == c2[1])):
            return True
    return False


#definição cesta para aparar as maçãs
bascket = [(200,590),(210,590),(220,590)]
bascket_skin = pygame.Surface((10,10))
bascket_skin.fill((255,255,255))


locationBascket = 200

#definição maçã
apple_x = on_grid_random()
apple_y = 0
apple = pygame.Surface((10,10))
apple.fill((255,0,0))

clock = pygame.time.Clock()


#Loop infinito onde os eventos acontecem
while True:

    #tempo cada ciclo no loop
    clock.tick(20)

    #Encerra o processo do jogo quando clicar em fechar a janela
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()

    #mover cesta na horizontal
    if event.type == KEYDOWN:
        if event.key == K_LEFT:
            if (locationBascket > 20):
                locationBascket = locationBascket - 20
            bascket = [(locationBascket ,590),(locationBascket - 10,590),(locationBascket - 20,590)]
        if event.key == K_RIGHT:
            if(locationBascket<590):
                locationBascket = locationBascket + 20
            bascket = [(locationBascket , 590), (locationBascket - 10, 590), (locationBascket - 20, 590)]
        if event.key == K_UP:
            print(locationBascket)

    if collision(bascket,(apple_x,apple_y)):
        apple_y = 0
        apple_x = on_grid_random()
        apple_pos = (apple_x, apple_y)
        score_value = score_value + 1
        total = total + 1

    #definindo o limite da tela para começar a cair outra maça
    if (apple_y == 600):
        apple_y = 0
        apple_x = on_grid_random()
        apple_pos = (apple_x, apple_y)
        total = total + 1

    #preenchendo a tela com a cor desejada em rgb
    screen.fill((150, 190, 0))
    #mostrando a maça na tela
    screen.blit(apple, (apple_x, apple_y))
    #mostrando a cesta na tela
    for pos in bascket:
        screen.blit(bascket_skin, pos)

    apple_y = apple_y + 10

    show_score(textX, textY)
    pygame.display.update()